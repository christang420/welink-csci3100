# WeLink

一个树洞型Web App，灵感来自CU转换器

---

## Done

1. 完成了【发帖】【看帖】【回复】三个页面的HTML基本构建
2. 用jQuery做了一下【顶部导航栏】的reuse
3. 找了一个备选的icon


---

## To-do

1. （超多。慢慢列）


---

## Useful links

1. [介绍Bootstrap基本操作的Tania博客，写的很清楚](https://www.taniarascia.com/what-is-bootstrap-and-how-do-i-use-it/)
2. [Bootstrap官方的组件基本介绍](https://getbootstrap.com/docs/3.3/components/)
3. [Bootstrap官方的表单等组件介绍，做register部分应该用到](https://getbootstrap.com/docs/4.0/components/forms/)
4. [Quora上关于如何reuse HTML组件的介绍(jQuery)，点进博客链接看](https://www.quora.com/What-is-the-best-way-to-create-reusable-HTML-templates-components-like-nav-bar-footer-customer-comments-that-can-be-reused-across-the-site)
5. [一些social media的图标](https://www.w3schools.com/icons/fontawesome_icons_brand.asp)

